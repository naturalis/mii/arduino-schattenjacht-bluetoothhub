#ifndef Helmdata_H
#define Helmdata_H

#include "Arduino.h"    

#define MaxObj 15

class HelmData
{
   public:

  HelmData();

  int ObjFound[MaxObj]; // Array waarin wordt bijgehouden weke objecten gevonden zijn.
  void storeVoltage(float V);
  void storeTemp(float T);
  void storeGauge(int G);
  void storePerc(float P);
  void storeMsgID(int M);
  void storetxPower(int P);
  void storeObj(int Obj);
  int GetObj(int n);
  float getVoltage(void);
  float getTemp(void);
  int getGauge(void);
  float getPerc(void);
  int getMsgID(void);
  int gettxPower(void);
  boolean FoundObj(void);
  boolean HasChanged(boolean Clear);

  private:
  boolean changed; // Deze vlag wordt gezet als een waarde wordt aangepast.
  float Voltage;
  float Temperatuur; // Temperatuur in de coulomb meter.
  int Gauge; // stand van de teller in coloumb meter
  float BattPerc; // Percentage geeft aan hoe vol / leeg de batterij is.
  int msgID; // Elke boodschap heeft een eigen id. Dit on dubbel uitlezen te voorkomen
  int txPower; // Signaalsterkte van ontvangen boodschap. 
 
 
 
};

#endif