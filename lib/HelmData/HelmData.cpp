#include "Arduino.h"
#include "HelmData.h"

HelmData::HelmData()
{
	changed = false;
}	

void HelmData::storeVoltage(float V)
{
Voltage = V;
changed = true;
}

void HelmData::storeTemp(float T)
{
Temperatuur = T;
changed = true;
}
void HelmData::storeGauge(int G)
{
Gauge = G;
changed = true;
}

void HelmData::storePerc(float P)
{
BattPerc = P;
changed = true;
}
void HelmData::storeMsgID(int M)
{
msgID = M;
changed = true;
}
void HelmData::storetxPower(int P)
{
txPower = P;
changed = true;
}

void HelmData::storeObj(int Obj)
{
ObjFound[Obj]++; 
changed = true;
}

int HelmData::GetObj(int n)
{
return ObjFound[n];
}

float HelmData::getVoltage(void)
{
return Voltage;
changed = false;
}
float HelmData::getTemp(void)
{
return Temperatuur;
changed = false;
}

int HelmData::getGauge(void)
{
return Gauge;
changed = false;
}

float HelmData::getPerc(void)
{
return BattPerc;
changed = false;
}
int HelmData::getMsgID(void)
{
return msgID;
changed = false;
}

int HelmData::gettxPower(void)
{
return txPower;
changed = false;
}

boolean HelmData::FoundObj(void)
{
int i;
boolean Found = false;
for(i=0;i<MaxObj;i++)
{
  if((ObjFound[i]) != 0)
	Found = true;
}
return Found;
}



boolean HelmData::HasChanged(boolean Clear)
{
boolean Chngd;
Chngd = changed;
if(Clear)
  changed = false;
return Chngd;
}

