
#include "Arduino.h"
#include "SWTimer.h"



SWTimer::SWTimer()
{
  _TimerVar = 0;
  _TimerFlag = false;
}

void SWTimer::Set(uint32_t TV)
{
  _TimerVar = TV;
  _TimerFlag = false;
}

uint32_t SWTimer::Get()
{
  return _TimerVar;
}

boolean SWTimer::Expired() // Functie leest de timerflag. 
{
  if(_TimerFlag)
  {
//    _TimerFlag = false;
	return true;
  }
  else
    return false;
}

void SWTimer::Halt() // Timer n is halted
{
  _TimerVar = 0;
  _TimerFlag = false;
}

void SWTimer::Update()
{
  if(_TimerVar > 0)
  {
    _TimerVar--;
	if(_TimerVar == 0)
	  _TimerFlag = true;
   }  
}







