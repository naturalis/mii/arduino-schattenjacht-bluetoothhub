
#include "Arduino.h"

#ifndef SWTimer_h
#define SWTimer_h



class SWTimer
{
  public:
    SWTimer();
    void Set(uint32_t TV);
    uint32_t Get();
	boolean Expired(); // Functie leest de timerflag en stijkt deze eveneens. 
	void Halt(); // Timer n is halted
	void Update();
  private:
    uint32_t _TimerVar;
	boolean _TimerFlag;
};

#endif