/* ======================================================================================================================================
===   Naturalis Zaal Aarde / Schatkamer helmpjes / BLE scanner 
===   
===   Deze arduino sketch voert elke seconde een Bluetooth Low Energy scan uit. (BLE role = Observer)
===   De informatie die hiermee verzamelt wordt wordt met een instelbare interval via een Ethernetverbinding naar een server verzonden.
===
===   Hardware: Olimex ESP32-PoE -iso
===
===   Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleScan.cpp
===
===   Dipswitches:
===   Dipswitch 1: ON = Stuur "Found-object" messages naar PharosHost
===   Dipswitch 2: ON = Stuur "Found-object" records naar TIM

=========================================================================================================================================*/

//==========================================================================
// Includes
//==========================================================================

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include <Wire.h>  // Only needed for Arduino 1.6.5 and earlier
#include <ETH.h>
#include <WiFi.h>
#include <HardwareSerial.h> // ESP32 serial library
#include <ArduinoJson.h>
#include "SWTimer.h"  // Software timer library
#include "time.h"
#include "HelmData.h"
//==========================================================================
//Defines
//==========================================================================


// == I/O ports
#define Rx_B GPIO_NUM_13
#define Tx_B GPIO_NUM_14

#define ETH_CLK_MODE ETH_CLOCK_GPIO17_OUT
#define ETH_PHY_POWER 12
static bool eth_connected = false;


#define BUTTON_PRESSED()  (!digitalRead (34))

#define MaxHelm 17
#define MaxObj 15

// Software timer configuratie
#define MaxTimers 5
#define SendStatusTimer 0
#define NTPClientTimer 1
#define TestTimer 2


//#define ExporterHost "145.136.241.35"
//#define ExporterPort 9111

#define ExporterHost "192.168.178.69"
#define ExporterPort 1280

//#define PharosHost "10.133.1.1"
//#define PharosPort 8000

#define PharosHost "192.168.178.69"
#define PharosPort 1280

String OutStr;
//
//#define SCL 4
//#define SDA 5
//

#define LED 5  // 


// Message type bits
#define B_Obj 0x01
#define B_VBat 0x02
#define B_Gauge 0x04
#define B_Temp 0x08
#define B_Perc 0x10

// Dipswitch
#define Dip_1 32
#define Dip_2 33
#define Dip_3 36
#define Dip_4 39

#define GetDipSw1()  (!digitalRead (Dip_1))
#define GetDipSw2()  (!digitalRead (Dip_2))
#define GetDipSw3()  (!digitalRead (Dip_3))
#define GetDipSw4()  (!digitalRead (Dip_4))

//==========================================================================
// Memory declarations
//==========================================================================

const char* ntpServer = "ntp.xs4all.nl";
const long  gmtOffset_sec = 3600;
// int   daylightOffset_sec = 3600;
int   daylightOffset_sec = 0;
boolean ClockReady = false;

boolean Dip1 = HIGH; // Status van Dipswitch 1
boolean Dip2 = HIGH; // Status van Dipswitch 2
boolean Dip3 = HIGH; // Status van Dipswitch 3
boolean Dip4 = HIGH; // Status van Dipswitch 4

uint32_t freeMem;

const int wdtTimeout = 5000;  //time in ms to trigger the watchdog
HardwareSerial SerialB(1); // serieele poort voor Pharos


String MyJSONString[MaxHelm]; // array van string 

boolean flfl = false; // debug variable
int scanTime = 1; //In seconds

//========= Hardware timers
hw_timer_t * timer0 = NULL;
hw_timer_t * timer1 = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;


 // ========= Software timers
SWTimer SW_Timer[MaxTimers]; // Array van SWTimer instanties

// ======== Declaratie van helm data array ========
HelmData HelmDatArr[MaxHelm]; // Array van HelmData instanties
 


//==========================================================================
// ==== function prototypes
//==========================================================================

void IRAM_ATTR onTimer0();    // systeem timer
void IRAM_ATTR resetModule(); // Watchdog timer


// ===============  Time functions  =======================

void printLocalTime()
{
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    ESP.restart();
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}


void getTimeFromNTP(void)
{
  //init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
 // configTzTime()
  printLocalTime();

}


//  =============== End of time functions =================






void WiFiEvent(WiFiEvent_t event)
{
  switch (event) {
    case SYSTEM_EVENT_ETH_START:
      Serial.println("ETH Started");
      //set eth hostname here
      ETH.setHostname("Helmen_Host");
      Serial.print(ETH.macAddress());
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println("ETH Connected");
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.print("ETH MAC: ");
      Serial.print(ETH.macAddress());
      Serial.print(", IPv4: ");
      Serial.print(ETH.localIP());
      if (ETH.fullDuplex()) {
        Serial.print(", FULL_DUPLEX");
      }
      Serial.print(", ");
      Serial.print(ETH.linkSpeed());
      Serial.println("Mbps");
      eth_connected = true;
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println("ETH Disconnected");
      eth_connected = false;
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println("ETH Stopped");
      eth_connected = false;
      break;
    default:
      break;
  }
}

void SendWlanMessage(const char *host, uint16_t port, String MyStr)
{
#define BfZ 255  
  if( eth_connected)
  {
  char Buf[BfZ];
  int Count = 0;
  Serial.print("\nconnecting to ");
  Serial.print(host);
  Serial.print("  -  ");
    Serial.println(port);
    
  WiFiClient client;
  if (!client.connect(host, port)) {
    Serial.println("connection failed");
//    return;
  }
  MyStr.concat("\r\n");  // voeg cr&lf toe
  MyStr.toCharArray(Buf,BfZ);
  
  client.printf(Buf, host);
  while (client.connected() && !client.available() && Count++ < 100  ){delay(1);Count++;};
 
  while (client.available()) 
    Serial.write(client.read());
  Serial.println("closing connection\n");
  client.stop();
  delay(250); // 
  }
}




// =================================================================================================
//== JSON Encoding 
// =================================================================================================

String buildJsonStatusMsg(int ID) 
{
  String MyStr;
  int i;
  StaticJsonBuffer<300> jsonBuffer;

  JsonObject& root = jsonBuffer.createObject();
    root["MsgType"] = "Status";
    root["HelmetNr"] = ID;

    root["BatteryVoltage"] = HelmDatArr[ID].getVoltage() ;
    root["BatteryPerc"] = HelmDatArr[ID].getPerc();
    root["ChargerTemp"] = HelmDatArr[ID].getTemp();
    if((HelmDatArr[ID].FoundObj()) && (GetDipSw2())) // Als er data is en de dipswitch 2 staat op "ON"
    {
      JsonArray& data = root.createNestedArray("Objects");
      for(i=1;i<MaxObj;i++)
      {
        data.add(HelmDatArr[ID].GetObj(i));
        HelmDatArr[ID].ObjFound[i] = 0;  
      }  
    }
    root.printTo(MyStr);
   
    return MyStr;
}

void SendStatus()
{
 int i;
boolean NoData = true; // flag om bij te houden of alle records leeg zijn.
  for(i=1;i<MaxHelm;i++)
  {
    if(HelmDatArr[i].HasChanged(true)) // HasChanged(true) betekent changed flag wordt gewist na lezen
    {
      NoData = false; // Flag = false want er is data
      MyJSONString[i] = buildJsonStatusMsg(i);
      Serial.print("Jsonstring: ");
      Serial.println(MyJSONString[i]);
    }
  }
  if(NoData == false)
  {
    Serial.println("SendMessage() uitvoeren");
 // Check of de strings in MyJSONString[] data bevatten. Zo ja verstuur deze via ethernet.
    for(i=1;i<MaxHelm;i++)
    {
      if(MyJSONString[i].length() > 0)
      {
        if (eth_connected) 
        {
          SendWlanMessage(ExporterHost, ExporterPort,MyJSONString[i]);
//          Serial.println(MyJSONString[i]);
          MyJSONString[i] = String(""); 
        }
        
      }
    
    }
  }  
  else
    Serial.println("Geen nieuwe data om te verzenden");
}





// ===================================================================
// =========================== Timers ================================
// ===================================================================

void IRAM_ATTR onTimer0() // wordt 10 x per seconde uitgevoerd
{
 int i;
 
   portENTER_CRITICAL_ISR(&timerMux);
//---------------update software Timers:    
   for(i=0;i<MaxTimers;i++)
   {
     SW_Timer[i].Update();
   } 
   portEXIT_CRITICAL_ISR(&timerMux);
}    
// ============= End of Timer0_IRQ


// =============== Watchdog timout routine  ===========================
// Onderstaande functie wordt uitgevoerd als de watchdog timer niet op tijd ge-reset is.
// Het systeem wordt opnieuw opgestart.
void IRAM_ATTR resetModule() 
{
//  Serial.println(" ==============================Watchdog reset!===============================");
//  delay(100);
 
  delay(2000);
  ets_printf("================== reboot ======================\n");
  esp_restart();
}



void DoTimers(void)
{
 
   if(SW_Timer[SendStatusTimer].Expired()) 
   {
      Serial.println("SendStatus");
      SW_Timer[SendStatusTimer].Set(18000); // Zet de SendStatusTimer op 30 minuten
      OutStr = "SendStatus";
      if (eth_connected) 
      {
        SendStatus();
      }   
   }

   if(SW_Timer[TestTimer].Expired())
   {
      freeMem = ESP.getFreeHeap();
      Serial.print("--------------------------------------------------------Heap : ");
      Serial.println(freeMem);
  
     SW_Timer[TestTimer].Set(100); // 
   }

   if(SW_Timer[NTPClientTimer].Expired())
   {
      if (eth_connected) 
      {
 //       getTimeFromNTP(); // Haal tijd op  bij NTP Server
 //       ClockReady = true;
      }  
      SW_Timer[NTPClientTimer].Halt(); 
   }
}   



void ParseMessage(String MyStr)
{
int MsgID; // 
byte MesgTyp;
 //String MyStr;
  String HStr;
  int MyInt;
  int HelmID = 0;
  int ObjID = 0;
  float MyTemp;
  float MyVBat;
  uint16_t MyGauge;
  
  int p;
  int p2;
  int i;

  MesgTyp = 0x00; 
  p = MyStr.indexOf("H_");  // Check of "H_:" in de advertising info zit. 
  if (p != -1)
  {
    HStr = MyStr.substring(p+2,32); // HelmID in HStr 
    HelmID = HStr.toInt(); // 
    if((HelmID > 0) && (HelmID < MaxHelm)) // Valid HelmID?
    {
      p = MyStr.indexOf("/");  // Check of de MsgID verschilt van de vorige. 
      if (p != -1)
      {
        HStr = MyStr.substring(p+1,32); // MsgID in HStr 
        MyInt = HStr.toInt(); //Doen: Check of het een valid int is
        if(HelmDatArr[HelmID].getMsgID() != MyInt) // Is het een nieuwe boodschap?
        {
          HelmDatArr[HelmID].storeMsgID(MyInt); // Update MsgID
         
          p = MyStr.indexOf("Obj:");  // Check of "Obj:" in de advertising info zit. 
          if (p != -1)
          {
            MesgTyp |= B_Obj;
            HStr = MyStr.substring(p+4,32); // ObjectID in HStr 
            ObjID = HStr.toInt(); // check validiteit
            HelmDatArr[HelmID].storeObj(ObjID); // Sla op in objecten array
          }

          p = MyStr.indexOf("VBat:");  // Check of "VBat:" in de advertising info zit. 
          if (p != -1)
          {
            MesgTyp |= B_VBat; 
            HStr = MyStr.substring(p+5,32); // ObjectID in HStr 
            HelmDatArr[HelmID].storeVoltage(HStr.toFloat()); // check validiteit
            p = MyStr.indexOf("Address:");  // Check of "Obj:" in de advertising info zit. 
            if (p != -1)
            {
              p2 = MyStr.indexOf(",",p+12);  //
             // MesgTyp |= B_VBat; 
             if(p2 != -1)
             {
              HStr = MyStr.substring(p+12,p2); // ObjectID in HStr 
           //   Serial.println(HStr);
            //  HelmDatArr[HelmID].storeVoltage(HStr.toFloat()); // check validiteit
             }
            }
          }

          p = MyStr.indexOf("Temp:");  // Check of "Temp:" in de advertising info zit. 
          if (p != -1)
          {
            MesgTyp |= B_Temp; 
            HStr = MyStr.substring(p+5,32); // ObjectID in HStr 
            HelmDatArr[HelmID].storeTemp(HStr.toFloat()); // check validiteit
          }
          
          p = MyStr.indexOf("Gauge:");  // Check of "Gauge:" in de advertising info zit. 
          if (p != -1)
          {
            MesgTyp |= B_Gauge; 
            HStr = MyStr.substring(p+6,32); // ObjectID in HStr 
            HelmDatArr[HelmID].storeGauge(HStr.toInt()); // check validiteit
          }

          p = MyStr.indexOf("Bat%:");  // Check of "Bat%:" in de advertising info zit. 
          if (p != -1)
          {
            MesgTyp |= B_Perc; 
            HStr = MyStr.substring(p+5,32); // ObjectID in HStr 
            HelmDatArr[HelmID].storePerc(HStr.toFloat()); // check validiteit

            p = MyStr.indexOf("txPower:");  // Check of "txPower:" in de advertising info zit. 
              if (p != -1)
              {
 //              Serial.print("MyStr =  ");
 //              Serial.println(MyStr);              

 //               Serial.print("p =");
 //               Serial.println(p);
                HStr = MyStr.substring(p+8); // txPower HStr 
 //               Serial.print("HStr = ");
 //               Serial.println(HStr);
                HelmDatArr[HelmID].storetxPower(HStr.toInt()); // check validiteit
              }
          }
          


          if(MesgTyp !=0)
          {
             Serial.print("Helm: ");
             Serial.print(HelmID);
             Serial.print(" ");
          }
          
          if(MesgTyp & B_Obj)
          { 
             if (ObjID <10)
               OutStr = "OBJ0";
             else   
               OutStr = "OBJ";
             OutStr.concat(ObjID);

             SerialB.print(OutStr);  // output voor Pharos
             Serial.print(OutStr);   // output serieele poort.
             if(GetDipSw1()) // Als Dipswitch 1 op "ON" staat stuur dan commando naar het Pharossysteem
              SendWlanMessage(PharosHost, PharosPort,OutStr); // Output 
             Serial.print("Objects: ");
             for(i=1;i<MaxObj;i++)
             {
               Serial.print(HelmDatArr[HelmID].ObjFound[i]);
               Serial.print(" ");
             }

          }
          if(MesgTyp & B_VBat)
          {
            Serial.print(" VBat: ");
            Serial.print(HelmDatArr[HelmID].getVoltage());
          }
          if(MesgTyp & B_Temp)
          {
            Serial.print(" Temp: ");
            Serial.print(HelmDatArr[HelmID].getTemp());
          }
          if(MesgTyp & B_Gauge)
          {
            Serial.print(" Gauge: ");
            Serial.print(HelmDatArr[HelmID].getGauge());
          }
          if(MesgTyp & B_Perc)
          {
            Serial.print(" Batt%: ");
            Serial.println(HelmDatArr[HelmID].getPerc());
//            Serial.print(" txPower: ");
//           Serial.println(HelmDatArr[HelmID].gettxPower());
          }          
          Serial.println();
  
        }  
      }      
    }
  }
}
//=======================

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks 
{
  String MyStr;
  String HStr;
  int MyInt;
  int HelmID;
  int ObjID;
  float MyTemp;
  float MyVBat;
  uint16_t MyGauge;
  
  int p;
    void onResult(BLEAdvertisedDevice advertisedDevice) 
    { 
      MyStr = advertisedDevice.toString().c_str();
 //    Serial.println(MyStr);
      digitalWrite(LED,HIGH);
 
      ParseMessage(MyStr);
    }  
};

//========================


// ==================================================================================================
//   S E T U P   
// ==================================================================================================

void setup() 
{
int i;
  pinMode (34,INPUT);
  pinMode (Dip_1, INPUT);
  pinMode (Dip_2, INPUT);
  pinMode (Dip_3, INPUT);
  pinMode (Dip_4, INPUT);

  Serial.begin(115200); // USB serieele poort
  SerialB.begin(115200, SERIAL_8N1, Rx_B, Tx_B); // Serieele poort voor Pharos lichtsturing
  Serial.println("ESP_BLE_Scanner");   
   
  BLEDevice::init("");
  pinMode(LED,OUTPUT);

//  Wire.begin(SDA,SCL); 

  // ======== Initialize Timer0 interrupt =========================
  // define frequency of interrupt
   timer0 = timerBegin(0, 80, true);
  // define the interrupt callback function
   timerAttachInterrupt(timer0, &onTimer0, true);
  // start the timer
  // Set alarm to call onTimer function every 1/10 second (value in microseconds).
  // Repeat the alarm (third parameter)
  timerAlarmWrite(timer0, 100000, true);
  timerAlarmEnable(timer0);
//==================================================================


// ======== Initialize Timer1 interrupt =========================
  timer1 = timerBegin(1, 80, true);                  //timer 1, div 80
  timerAttachInterrupt(timer1, &resetModule, true);  //attach callback
  timerAlarmWrite(timer1, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer1);                          //enable interrupt
//==================================================================


// ================ Initialiseer SW_Timers  ========================
  for(i=0; i<MaxTimers; i++)  // start alle SW timers
    SW_Timer[i].Set(100);
  SW_Timer[SendStatusTimer].Set(6000);  // Start SendStatusTimer (10 min.)
  SW_Timer[NTPClientTimer].Set(50); // 5 sec
  SW_Timer[TestTimer].Set(40);

// ================ Initialiseer Netwerk  ========================
  WiFi.onEvent(WiFiEvent);
  ETH.begin();
//==================================================================

// ================= Initialize Dip switch =========================
  if(GetDipSw1() ) Serial.println("Dip1 = ON"); else Serial.println("Dip1 = OFF");
  if(GetDipSw2() ) Serial.println("Dip2 = ON"); else Serial.println("Dip2 = OFF");
  if(GetDipSw3() ) Serial.println("Dip3 = ON"); else Serial.println("Dip3 = OFF");
  if(GetDipSw4() ) Serial.println("Dip4 = ON"); else Serial.println("Dip4 = OFF");
}


void TriggerPharos(int N)
{
  if(N<10)
    OutStr = "OBJ0";
  else   
    OutStr = "OBJ";
    OutStr.concat(N);

      SerialB.print(OutStr);
      Serial.print("Sending: ");
      Serial.println(OutStr);
      if( GetDipSw1())
        SendWlanMessage(PharosHost, PharosPort,OutStr); // Output 
}


void BuildTestRecord()
{
int MyId = 16;
  HelmDatArr[MyId].storeVoltage(3.45); 
  HelmDatArr[MyId].storeTemp(20.5); 
  HelmDatArr[MyId].storePerc(60.70);
}


// ==================================================================================================
//   Loop()
// ==================================================================================================

void loop() {
  delay(2);
  
  timerWrite(timer1, 0); //reset timer (feed watchdog)

  BLEScan* pBLEScan = BLEDevice::getScan(); //create new scan
  MyAdvertisedDeviceCallbacks myCallbacks;
  pBLEScan->setAdvertisedDeviceCallbacks(&myCallbacks);
  pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
  digitalWrite(LED,LOW);

  BLEScanResults foundDevices = pBLEScan->start(1);

   DoTimers();
  if(BUTTON_PRESSED())
    {
      Serial.println("Wacht op knop loslaten");
      while(BUTTON_PRESSED()); // wacht tot knop is losgelaten.
//     Serial.println("Sending message to Pharos");
//      TriggerPharos(16);
      SendStatus();
    }
}
