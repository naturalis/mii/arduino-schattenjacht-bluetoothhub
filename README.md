# arduino-schattenjacht-bluetoothhub

Dit is een ESP32 bordje die via POE op het netwerk is aangesloten. Deze arduino sketch voert elke seconde een Bluetooth Low Energy scan uit. (BLE role = Observer)
De informatie (status van ieder helmpje) die hiermee verzamelt wordt wordt met een instelbare interval via een Ethernetverbinding (tcp) naar een server die de gegevens vervolgens doorzet naar Prometheus.

Hardware: Olimex ESP32-PoE -iso

Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleScan.cpp

Dipswitches:
Dipswitch 1: ON = Stuur "Found-object" messages naar PharosHost
Dipswitch 2: ON = Stuur "Found-object" records naar TIM


## No OTA

Omdat de firmware die gecompileerd wordt anders niet past moest
wel worden aangegeven dat de 'over-the-air' update functionaliteit
van de ESP32 niet langer gebruikt moet worden. Dit doe je met de
`no_ota.csv` en de extra configuratie in platformio.ini:

```
board_build.partitions = no_ota.csv
```
